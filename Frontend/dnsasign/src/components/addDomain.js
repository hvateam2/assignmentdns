import React, { useState } from 'react';
import axios from 'axios';
import './addDomain.css';

const Adddomain = ({ token }) => {
    console.log(token);

    const [formData, setFormData] = useState({
        domain: '',
        type: 'A',
        value: '',
        ttl: ''
    });

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post(
                'http://localhost:4000/addDomain',
                formData,
                {
                    headers: {
                        Authorization: `Bearer ${token}` // Include token in the request headers
                    }
                }
            );
            console.log(response.data);
            setFormData({ domain: '', type: 'A', value: '', ttl: '' });
        } catch (error) {
            console.error('Error:', error);
        }
    };

    return (
        <div>
            <section className='formBox'>
                <form onSubmit={handleSubmit}>
                    <label>Domain Name</label>
                    <input type='text' name='domain' value={formData.domain} onChange={handleChange} />
                    <label>Type</label>
                    <select name='type' value={formData.type} onChange={handleChange}>
                        <option value="A">A</option>
                        <option value="AAAA">AAAA</option>
                        <option value="MX">MX</option>
                        <option value="TXT">TXT</option>
                    </select>
                    <label>Value</label>
                    <input type='text' name='value' value={formData.value} onChange={handleChange} />
                    <label>TTL</label>
                    <input type='text' name='ttl' value={formData.ttl} onChange={handleChange} />
                    <button type='submit' className='addSubmit'>Add</button>
                </form>
            </section>
        </div>
    );
};

export default Adddomain;
