import React, { useState } from 'react';
import axios from 'axios';
import './domain.css';
import Adddomain from './addDomain.js';

function ProductList({ token }) {
  const [title, setTitle] = useState('');
  const [products, setProducts] = useState([]);
  const [showAddDomain, setShowAddDomain] = useState(false); // State to manage the visibility of Adddomain component

  const handleSearch = async () => {
    try {
      const response = await axios.post(
        'http://localhost:4000/addDomain',
        //{ title },
        {
          headers: {
            Authorization: ` ${token}`
          }
        }
      );
      const data = response.data;
      setProducts(data);
    } catch (error) {
      console.error('There was a problem fetching the data:', error.message);
    }
  };

  // Function to toggle the visibility of Adddomain component
  const toggleAddDomain = () => {
    setShowAddDomain(!showAddDomain);
  };

  return (
    <div>
      <div className='heaidngBox'>
        <h1 className='headingName'>Domain Name System</h1>
      </div>
      <div className='buttoncontainer'>
        {/* Button to toggle visibility of Adddomain component */}
        <button className='domainButtons' onClick={toggleAddDomain}>Add Domain</button>
        <button className='domainButtons'>check Details</button>
        <button className='domainButtons'>Edit DNS</button>
        <button className='domainButtons'>Delete DNS</button>
      </div>

      {/* Render Adddomain component conditionally based on state */}
      {showAddDomain && <Adddomain  token={token} />}
    </div>
  );
 
}

export default ProductList;
