import React, { useState } from 'react';
import axios from 'axios';
import './login.css'

function AmazonSignIn({ onSignInSuccess }) {
    const [msg, setMsg] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        try {
            const response = await axios.post('http://localhost:4000/login', { email, password });
            console.log(response)
        
            if (response.data.message === 'user verified') {
                setMsg('Email and password are verified.');
                onSignInSuccess(response.data.token);
            } else {
                setMsg('Email or password does not exist in the database.');
            }
        } catch (error) {
            if (error.response) {
                setError('Error: ' + error.response.data.message);
            } else if (error.request) {
            
                setError('Network Error: Please check your internet connection or try again later.');
            } else {
    
                setError('Error: ' + error.message);
            }
            console.error('Error:', error);
        }
    };

    return (
        <div>
        <h1>DNS management</h1>
        <div className="container">
           
            <div className="signin-container" >
                <h2>Sign-In</h2>
                <form onSubmit={handleSubmit}>
                    <label htmlFor="email">Email</label>
                    <input type="email" id="email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    <label htmlFor="password">Password</label>
                    <input type="password" id="password" name="password" value={password} onChange={(e) => setPassword(e.target.value)} required />

                    <button type="submit">Check Details</button>
                </form>
                <p>{msg}</p>
                <p>{error}</p>
            </div>
        </div>
        </div>
    );
}

export default AmazonSignIn;
