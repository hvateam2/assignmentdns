import React, { useState } from 'react';
import './App.css';
import AmazonSignIn from './components/user/login';
import ProductList from './components/domain';
import Adddomain from './components/addDomain';

function App() {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const [token, setToken] = useState(null);//store token in state

  const handleSignInSuccess = (token) => {
    setIsSignedIn(true);
    setToken(token); //set token get from sign in
  };

  return (
    <div className="App">
  {isSignedIn ? (
    <>
      <ProductList token={token} />
    </>
  ) : (
    <AmazonSignIn onSignInSuccess={handleSignInSuccess} />
  )}
</div>

  );
}

export default App;
