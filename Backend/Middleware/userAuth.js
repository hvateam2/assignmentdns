const jwt=require('jsonwebtoken')
const secretKey='pri123'
function verifyUser(req,res,next){
    const {token}=req.header('Authorization')
    if(!token){
        res.status(400).json({message:"Bad Request"})
    
    }
    else{
        jwt.verify(token,secretKey,(error,data)=>{
            if(error){
                res.status(403).json({message:"forbidden token"})
            }
            else{
                req.user=data.email
                next()
            }
        })
    }
}
module.exports=verifyUser