const express=require('express')
const {Router}=require('express')
const jwt=require('jsonwebtoken')
const secretKey='pri123'
const getUserDB=require('../modals/userData')
const router=Router()
router.use(express.json())

router.post('/login',async(req,res)=>{
    const {email,password}=req.body
    try{
    const checkDetails=await getUserDB.findOne({email:email,password:password})
    if(!checkDetails){
        res.status(400).json({message:"data not found"})
    }
    else{
        const token=jwt.sign({email:getUserDB.email},secretKey,{expiresIn:'5h'})
        res.status(200).json({message:"user verified",token})
    }
    }
    catch{
           res.status(500).json({message:"internal server error"})
    }

})
module.exports=router