const express=require('express')
const {Router}=require('express')
const router=Router()
const getUserModal=require('../modals/userData')
router.use(express.json())
router.post('/signup',async(req,res)=>{
    const {email,password}=req.body

    if(!email || !password){
        res.status(400).json({message:"data field missed"})
    }
    else{
        try{
           const data=new getUserModal({
            email,
            password
           })
           await data.save()
           res.status(201).json({message:"user data inserted successfuly"})
        }
        catch{
              res.status(500).json({message:"internal server error"})
        }

    }
    
})
module.exports=router