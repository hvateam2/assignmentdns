const express=require('express')
const Router=require(express)
const getAuth=require('../Middleware/userAuth')
const getDomainModal=require('../modals/domainDetails')
const router=Router()
router.use(express.json())
router.post('/domain',getAuth,async(req,res)=>{
const {domain,type,value,ttl}=req.body
if(!domain || !type || !value || !ttl){
    res.status(403).json({message:'some data field missed'})
}
else{
    try{
     const newDomain= new getDomainModal({
        domain,
        type,
        value,
        ttl
     })
     await newDomain.save()
     res.status(201).json({message:"new domain added successfully"})
    }
    catch(error)
    {
        res.status(500).json({message:"internal server error",error})
    }
}
})
module.exports=router