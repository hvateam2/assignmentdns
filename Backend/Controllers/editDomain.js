const express=require('express')
const {Router}=require('express')
const getDomainModel=require('../modals/domainDetails')
const router=Router()
router.post('/updateDomain',async(req,res)=>{
    const{value,newDomain,newType,newttl}=req.body
    try{
      const updateDomain= await getDomainModel.updateOne(
        {value},
        {
            $set: {
              domain:newDomain,
              type:newType,
              ttl:newttl  
            }
        }
      )
      res.status(200).json({message:"domain details updated successfully"})
      }
    catch{
     res.status(500).json({message:"internal server error"})
    }
})
module.exports=router
