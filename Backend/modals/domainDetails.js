const { string } = require('joi')
const mongoose=require('mongoose')
mongoose.connect('mongodb://127.0.0.1:27017/dnsDataBase')
const domainSchema=new mongoose.Schema({
    domain:{
        type:String,
        require:true
    },
    type:{
        type:String,
        enum: ['A', 'AAAA', 'MX', 'TXT'],
        require:true
    },
    value: {
        type: String,
        required: true
    },
    ttl: {
        type: Number,
        default: 3600 
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

const DNSRecord = mongoose.model('DNSRecord', domainSchema);

module.exports = DNSRecord;